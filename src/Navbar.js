
const Navbar = () => {
    return ( 
        <nav className="navbar">
            <h1>ToDo App</h1>
            <div className="links">
                <a href="Home">Home</a>
                <a href="New Todo">New Todo</a>
            </div>
        </nav>
     );
}
 
export default Navbar;