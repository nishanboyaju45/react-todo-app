import { useState } from "react";
import TodoList from "./TodoList";
import AddTodos from "./AddTodos";

const Home = () => {
    const [todos, setTodos] = useState([
        {
            id: 1,
            title: "Sample Todo",
            description: "Todo looks like this",
            completed: false

        }
    ]);

    const [id, setId] = useState(2);

    const handleDelete = (id) => {
        const newTodos = todos.filter(todo => todo.id !== id);
        setTodos(newTodos);
    }

    const handleCreate = (title, description) => {
        const todo = {
            'id': id,
            'title': title,
            'description': description,
            'completed': false

        };
        const newTodosList = [...todos, todo];
        setTodos(newTodosList);
        setId(id + 1);
    }

    const updateTodo = (e, id) => {
        const newTodos = todos;
        // debugger;
        const formattedValue = newTodos.map((todo) => {
            // console.log(todo.completed);
            // console.log(id);
            // console.log(e);
            let temp = todo;
            if (todo.id == id) {
                // debugger;

                temp.completed = e.target.checked;
            }
            return temp;
        });
        // debugger;
        setTodos(formattedValue);


    }


    return (
        <div className="home">
            <AddTodos handleCreate={handleCreate} />
            <h2>Remaining: {todos.filter((todo) => !todo.completed).length}</h2>
            <h2>Completed: {todos.filter((todo) => todo.completed).length}</h2>
            <div className="d-flex">

                <div className="me-auto">
                    <TodoList todos={todos.filter((todo) => !todo.completed)} handleDelete={handleDelete} updateTodo={updateTodo} header={"Remaining"} />
                </div>
                <div className="ms-auto">
                    <TodoList todos={todos.filter((todo) => todo.completed)} handleDelete={handleDelete} updateTodo={updateTodo} header={"Completed"} />
                </div>

            </div>
        </div>
    );
}

export default Home;