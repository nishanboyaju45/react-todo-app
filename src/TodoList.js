const TodoList = ({ todos, handleDelete, updateTodo, header }) => {
    return (
        <div className="todo-list">
            <h1><p>{header}</p></h1>
            
            {todos.map((todo) => (
                <div className="todo-preview" key={todo.id}>
                    <h2>{todo.title}</h2>
                    <p>{todo.description}</p>
                    <input type="checkbox" onChange={(e) => updateTodo(e, todo.id)} checked={todo.completed} />
                    <label>Done</label>
                    <button onClick={() => handleDelete(todo.id)} style={myStyle}>Delete</button>
                </div>
            ))}

        </div>
    );
}

const myStyle = {
    marginLeft: '10px',
    marginRight: '10px'
}
export default TodoList;