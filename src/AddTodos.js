import { useState } from "react";

const AddTodos = ({handleCreate}) => {

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    // const [deadline, setDeadline] = useState(new Date());
    const handleSubmit = (e) => {
        e.preventDefault();
        // const todo = {title, description};
        // console.log(todo);
        handleCreate(title, description);
        // const newList = todos.push(todo);
        // setTodosList(newList);

    }

    return (
        <div className="create">
            <form onSubmit={handleSubmit} >
                <label>Title:</label>
                <input type="text" required value={title} onChange={(e) => setTitle(e.target.value)} />

                <label>Description:</label>
                <textarea required value={description} onChange={(e) => setDescription(e.target.value)} ></textarea>

                {/* <label for="date">Deadline:</label>
                <input type="date" value={deadline} onChange={(e) => setDeadline(e.target.value)}></input> */}

                <button>Add Todo</button>
                {/* <p>{title}</p> */}
                {/* <p>{description}</p> */}
                {/* <p>{deadline}</p> */}
                
            </form>
        </div>
    );
}

export default AddTodos;