# React ToDo App

![alt text](https://gitlab.com/nishanboyaju45/react-todo-app/-/raw/master/public/screenshots/reactTodoApp.png)


## Features

* Add New ToDo
* Remaining ToDo and Completed ToDo blocks
* Delete existing ToDos from both remaining and completed blocks
* Mark Todo as done to shift the todo from 'Remaining' to 'Completed' and vice versa
* Total Remaining and Completed todos
